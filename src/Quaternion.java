import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;

/** Quaternions. Basic operations. */
public class Quaternion {

   private double a;
   private double b;
   private double c;
   private double d;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {

      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;

      // https://enos.itcollege.ee/~jpoial/algoritmid/quat/
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return this.a;
   }

   /** Imaginary part i of the quaternion.
    * @return imaginary part i
    */
   public double getIpart() {
      return this.b;
   }

   /** Imaginary part j of the quaternion.
    * @return imaginary part j
    */
   public double getJpart() {
      return this.c;
   }

   /** Imaginary part k of the quaternion.
    * @return imaginary part k
    */
   public double getKpart() {
      return this.d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */

   @Override
   public String toString() {
      String bPlus = this.b >= 0 ? "+" : "";
      String cPlus = this.c >= 0 ? "+" : "";
      String dPlus = (this.d >= 0 ? "+" : "");


      double k = 0.000000001;
      String aPart = Math.abs(this.a) < k ? "" : String.valueOf(this.a);
      String bPart = Math.abs(this.b) < k ? "" : bPlus + this.b + "i";
      String cPart = Math.abs(this.c) < k ? "" : cPlus + this.c + "j";
      String dPart = Math.abs(this.d) < k ? "" : dPlus + this.d + "k";

      String a = aPart + bPart + cPart + dPart;

      return a;
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {

      /*
      Kõige suurem probleem meetod valueOf, mis mõtab stringi sisse ja
      tekitab sellest Quaternion objekti.
      String tuleb analüüsida, vaata, kas vigu pole sees
      ja tagasi peegeldada vigane string.
       */

      if (s.equals("")) {
         return new Quaternion(0.0, 0.0, 0.0, 0.0);
      }

      stringValidation(s);  // enne kui midagi teen, valideerin sisendi.

      // Initializing a Dictionary
      Dictionary<String, Double> valuesDict = new Hashtable<>();

      valuesDict.put("r", 0.0);
      valuesDict.put("i", 0.0);
      valuesDict.put("j", 0.0);
      valuesDict.put("k", 0.0);

      String toList = "";
      String ignore = "ikj";
      String currentKey = "r";
      for (int i = 0; i < s.length(); i++) {

         String current = String.valueOf(s.charAt(i));

         if (ignore.contains(current)) {
            currentKey = current; // Just get current key and continue.
         }
         else if (current.equals("+")) {
            valuesDict.put(currentKey, Double.valueOf(toList));
            toList = "";

         }
         else if (current.equals("-") && i == 0) {  // kui - on stringi alguses
            toList = "-";
         }
         else if (current.equals("-")) {
            valuesDict.put(currentKey, Double.valueOf(toList));
            toList = "-";
         }
         else {
            toList += current;  // kui ei ole spliti koht, siis lisa sümbol stringi

         }
      }
      valuesDict.put(currentKey, Double.valueOf(toList));

//      System.out.println("r >> " + valuesDict.get("r"));
//      System.out.println("i >> " + valuesDict.get("i"));
//      System.out.println("j >> " + valuesDict.get("j"));
//      System.out.println("k >> " + valuesDict.get("k"));


      double r = valuesDict.get("r");
      double i = valuesDict.get("i");
      double j = valuesDict.get("j");
      double k = valuesDict.get("k");
      Quaternion a = new Quaternion(r, i, j, k);

      return a; // TODO!!!
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */

   public static void stringValidation (String s) {

      String x = "ijk";  // sisestatud stringist peab tekkima sellele substring.
      String substring = "";

      for (int i = 0; i < s.length(); i++) {
         if (x.contains(String.valueOf(s.charAt(i)))) {
            substring += String.valueOf(s.charAt(i));
         }
      }
      if (substring.length() > x.length()){
          throw new RuntimeException("MyError: Too many letters in string: " + s + ". Should be 3 but was " + substring.length() + ".");
      }

      if (!(x.contains(substring))) {
         throw new RuntimeException("MyError: Incorrent string used as an input: " + s);
      }
   }


   @Override
   public Object clone() throws CloneNotSupportedException {

      Quaternion clone = new Quaternion(this.a, this.b, this.c, this.d);
      return clone; // TODO!!!
   }

   /** Test whether the quaternion is zero.
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {

      /*
      Reaalarvude täpsus isZero. K == 0 on halb idee (mõttetu) ja ei tööta.
      Reaalarvud kujutuvad mingi täpsusega. Tuleb teha nii, et vaadata väiksem
      kui väike konstant. |k| < 0.0000001 näiteks. |a - b| < 0.00000001
      true, if the real part and all the imaginary parts are (close to) zero
       */

      double k = 0.00000001;

      if (Math.abs(this.a) > k) {
         return false;
      }
      if (Math.abs(this.b) > k) {
         return false;
      }
      if (Math.abs(this.c) > k) {
         return false;
      }
      if (Math.abs(this.d) > k) {
         return false;
      }
      return true;
   }

   /** Conjugate of the quaternion. Expressed by the formula
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {

      Quaternion result = new Quaternion(this.a, this.b * -1, this.c * -1, this.d * -1);

      return result; // TODO!!!
   }

   /** Opposite of the quaternion. Expressed by the formula
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {

      Quaternion result = new Quaternion(this.a * -1, this.b * -1, this.c * -1, this.d * -1);
      return result; // TODO!!!
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {

      Quaternion result = new Quaternion(
              this.a + q.getRpart(),
              this.b + q.getIpart(),
              this.c + q.getJpart(),
              this.d + q.getKpart()
      );

      return result; // TODO!!!
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {

      double a1 = a;
      double b1 = b;
      double c1 = c;
      double d1 = d;
      double a2 = q.getRpart();
      double b2 = q.getIpart();
      double c2 = q.getJpart();
      double d2 = q.getKpart();

      // (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i + (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k

      Quaternion result = new Quaternion((a1*a2-b1*b2-c1*c2-d1*d2), (a1*b2+b1*a2+c1*d2-d1*c2), (a1*c2-b1*d2+c1*a2+d1*b2), (a1*d2+b1*c2-c1*b2+d1*a2));
      return result; // TODO!!!
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {

      Quaternion result = new Quaternion(a*r, b*r, c*r, d*r);
      return result; // TODO!!!
   }

   /** Inverse of the quaternion. Expressed by the formula
    *      a/(a*a+b*b+c*c+d*d) + ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    *
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {

      if (this.isZero()) {
         throw new RuntimeException("Can not divide by zero in Quaternion " + this + " in method inverse().");
      }

      double bottom = (a*a + b*b + c*c + d*d);
      Quaternion result = new Quaternion ((a / bottom), (-b/bottom), (-c/bottom), (-d/bottom));
      return result; // TODO!!!
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {

      // Difference of quaternions. Expressed as addition to the opposite.

      Quaternion result = new Quaternion(
              this.a - q.getRpart(),
              this.b - q.getIpart(),
              this.c - q.getJpart(),
              this.d - q.getKpart()
      );

      return result;
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {

      if (q.isZero()) {
         throw new RuntimeException("Division by zero! Can not use Quaternion " + q + " in method divideByRight().");
      }

      Quaternion a = q.inverse();
      return this.times(a); // TODO!!!
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {

      if (q.isZero()) {
         throw new RuntimeException("Division by zero! Can not use Quaternion " + q + " in method divideByLeft().");
      }

      Quaternion a = q.inverse();
      return a.times(this);
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {

      Quaternion other = (Quaternion) qo;

      if (this.minus(other).isZero()) {
         return true;
      }

      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {

      Quaternion a = q.conjugate().times(this);
      Quaternion b = this.conjugate().times(q);
      Quaternion sum = a.plus(b);
      Quaternion result = new Quaternion(sum.a/2, sum.b/2, sum.c/2, sum.d/2);

      return result;
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() { // OK

      return java.util.Objects.hash(a, b, c, d); // TODO!!!
   }

   /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {

      /*
      public double norm()
      Norm of the quaternion. Expressed by the formula norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
      Returns:
      norm of this (norm is a real number)
       */

      return Math.sqrt(a*a+b*b+c*c+d*d);
   }

   /** Main method for testing purposes.
    * @param arg command line parameters
    */
   public static void main (String[] arg) {

//      Quaternion zero = new Quaternion (0., 0, 0., -0.);
//      Quaternion normal = new Quaternion (1., 3, 4., -2.);
//      zero.inverse();
//      normal.divideByRight(zero);
//      zero.divideByLeft(normal);

//      String s = "3+7i-5j";
//      valueOf(s);



      stringValidation("5+6i+7i+8k");  // i komponent esineb topelt
      stringValidation("5+6ii+7j+8k"); // i sümbol esineb topelt
      stringValidation("5+6j+7i+8k"); // i ja j vahetuses

      stringValidation("5+6i+7j+8k"); // ok

      // Panin eraldi meetodisse, et main oleks enda katsetuste tarbeks lühem.
//      teachersProgram(arg);


   }

   public static void teachersProgram(String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: "
              + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}